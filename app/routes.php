<?php

Route::any('/', 'LoginController@index');
Route::get('getTask/{id}', 'TasksController@getTask');
Route::get('tasks/getId/{id}', 'TasksController@getId');
Route::get('tasks/updateTask/{id}', 'TasksController@updateTask');
Route::post('log/{id}', 'LogController@log');
Route::resource('tasks', 'TasksController');
Route::resource('task', 'TaskController');
Route::resource('credentials', 'CredentialsController');
Route::resource('login', 'LoginController');