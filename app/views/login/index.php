<?php require base_path().'/app/views/header.php';?>
<div class="container">
  <div class="row">
    <div class="col-md-3 col-md-offset-4" style="margin-top: 70px">
        <?php echo Form::open(array('route' => 'login.index')); ?>
        <h2 class="form-signin-heading">Zaloguj</h2>
        <input type="text" name="username" class="form-control" placeholder="Login (dk)" autofocus required>
        <input type="password" name="password" class="form-control" placeholder="Hasło" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Zapamiętaj
        </label>
        <button class="btn btn-sm btn-primary btn-block" type="submit">Zaloguj</button>
      <?php echo Form::close(); ?>
    </div> 
  </body>
</html>