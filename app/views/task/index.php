<?php require base_path().'/app/views/header.php';?>
<?php require base_path().'/app/views/navbar.php';?>
<div class="container">
<div class="row">
<div class="col-md-7" style="margin-top: 70px">
<?php if(Session::has('flash_notice')): ?>
  <div class="alert alert-success">Zlecenie <b><?php echo Session::get('flash_notice') ?></b> dodane </div>
<?php endif ?>
<?php if(Session::has('login')): ?>
  <div class="alert alert-success"><b><?php echo Session::get('login') ?></b></div>
<?php endif ?>
  <?php 
  echo Form::open(array('route' => 'task.store'));
  echo Form::label('umowa', 'Nr abonenta/umowy');
  echo Form::text('umowa', null, array(
                      'class' => 'form-control',
                      'placeholder' => 'Nr abonenta/umowy',
                      'required' => 'required',
  ));
  echo '<br />';
  echo Form::label('pakiet', 'Nazwa pakietu');
  echo Form::text('pakiet', null, array(
                      'class' => 'form-control',
                      'placeholder' => 'Nazwa pakietu',
                      'required' => 'required',
  ));
  echo '<br />';
  echo Form::label('dodatkowe', 'Opcje dodatkowe');
  echo '<br />';
  echo Form::select('dodatkowe[]', array(
                  'HBO' => 'HBO', 
                  'FILMBOX' => 'FILMBOX',
                  'CINEMAX' => 'CINEMAX',
                  'EXTENSION_MEZZO' => 'EXTENSION_MEZZO',
                  'EXTENSION_TV_FRANCE' => 'EXTENSION_TV_FRANCE',
                  'HISTORY_HD' => 'HISTORY_HD',
                  'NOC' => 'NOC',
                  'REDLIGHT' => 'REDLIGHT',
                  'SUNDANCE' => 'SUNDANCE',

                  ), 
                null,
                array(
                  'class' => 'selectpicker',
                  'required' => 'required',
                  'title' => 'wybierz jedno lub kilka',
                  'multiple' => 'multiple',
  ));
  echo '<br /><br />';
  echo Form::label('uwagi', 'Uwagi');
  echo '<br />';
  echo Form::textarea('uwagi', null, array(
                        'class' => 'form-control',
                        'rows' => '3',
                        'placeholder' => 'Uwagi',
  ));
  echo '<br />';

  echo Form::submit('Zapisz', array(
                  'class' => 'btn btn-default',
                  'id'  =>  'insert',
                  'type'  => 'button',
  ));
    
  echo Form::close(); 

  ?>
</div>
</div>
</div>
</body>
</html>