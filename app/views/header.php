<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Addconnect BOSS</title>
 
    <link href="<?php echo URL::asset('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo URL::asset('assets/css/bootstrap-select.css') ?>" rel="stylesheet">
    <script src="<?php echo URL::asset('assets/js/jquery-2.0.3.min.js') ?>"></script>
    <script src="<?php echo URL::asset('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo URL::asset('assets/js/jquery.tablesorter.min.js') ?>"></script>
    <script src="<?php echo URL::asset('assets/js/bootstrap-select.min.js') ?>"></script>
    <script src="<?php echo URL::asset('assets/js/script.js') ?>"></script>
</head>