<?php require base_path().'/app/views/header.php';?>
<?php require base_path().'/app/views/navbar.php';?>
<div class="container">

 <br><br><br>
 
    <!-- <h1>
        tasksss <a href="<?php //echo URL::route('tasks.create') ?>" class="btn btn-success"><i class="icon-plus-sign"></i> Add new task</a>
    </h1> -->

    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          Wyświetl <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="forms.php?status=1">Otwarte</a></li>
          <li><a href="forms.php?status=2">Pobrane</a></li>
          <li><a href="forms.php?status=3">Zamknięte</a></li>
          <li class="divider"></li>
          <li><a href="forms.php?status=0">Wszystkie</a></li>
        </ul>
      </div><br /><br />
    <table id="myTable" class="table table-striped table-bordered table-condensed tablesorter">
        <thead>
            <tr>
                <th>#</th>
                <th>Nr umowy</th>
                <th>Pakiet</th>
                <th>Opcje</th>
                <th>Twórca</th>
                <th>Data</th>
                <th>Status</th>
                <th>Kontrolki</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks as $task): ?>
                <tr>
                    <td><?php echo  $task->id ?></td>
                    <td><!--<a href="<?php //echo  URL::route('tasks.show', $task->id) ?>"> --><?php echo  $task->nr_umowy ?></a></td>
                    <td><?php echo $task->pakiet ?></td>
                    <td><?php echo $task->opcje ?></td>
                    <td><?php echo $task->login_konsultanta ?></td>
                    <td><?php echo $task->created_at ?></td>
                    <td><?php echo $task->status ?></td>
                    <td><a href="<?php echo action('TasksController@getTask', $task->id)?>" class="btn btn-success btn-xs pull-left">Pobierz</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Edycja</h4>
        </div>
        <div class="modal-body">
          <div class="result"></div>
          <form action="/boss/view/forms.php" method="post" id="myForm" name="myForm">
            <div id="tempId"></div>
                <div class="form-group">
                  <label for="umowa">Nr abonenta/umowy</label>
                  <input name="umowa" type="text" class="form-control" id="umowa" placeholder="Nr abonenta/umowy" value= "" required>
                </div>
                <div class="form-group">
                  <label for="pakiet">Nazwa pakietu</label>
                  <input name="pakiet"  type="text"class="form-control" id="pakiet" placeholder="Nazwa pakietu" value= "" required>
                </div>
                <div class="form-group">
                  <label for="dodatkowe">Opcje dodatkowe</label><br />
                  <select name="dodatkowe" class="selectpicker" multiple title="Wybierz jedno lub kilka" value= "" required>
                      <option value="HBO">HBO</option>
                      <option value="FILMBOX">FILMBOX</option>
                      <option value="CINEMAX">CINEMAX</option>
                      <option value="EXTENSION_MEZZO">EXTENSION MEZZO</option>
                      <option value="EXTENSION_TV_FRANCE">EXTENSION TV FRANCE</option>
                      <option value="HISTORY_HD">HISTORY HD</option>
                      <option value="NOC">NOC</option>
                      <option value="REDLIGHT">REDLIGHT</option>
                      <option value="SUNDANCE">SUNDANCE</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="uwagi">Uwagi</label>
                  <textarea class="form-control" rows="3" name="uwagi" id="uwagi" value= ""></textarea>
                </div>
                <div class="checkbox">
                <label>
                  <input type="checkbox" id="check_id" >
                  Czy zamówienie wprowadzone do CGA?
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
              <button id="update" type="button" class="btn btn-primary">Zapisz</button>
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</body>
</html>