<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>L4 Site</title>
 
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">

 
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div>
    <div>
        <div>
            <div>
                <a href="<?php //URL::route('admin.taskss.index') ?>">L4 Sik,njte</a>
 
                <?php if (Sentry::check()): ?>
                    <ul>
                        <li><a href="<?php //URL::route('admin.taskss.index') ?>"><i></i> taskss</a></li>
                        <li><a href="<?php //URL::route('admin.taskss.index') ?>"><i></i> Articles</a></li>
                        <li><a href="<?php //URL::route('admin.logout') ?>"><i></i> Logout</a></li>
                    </ul>
                <?php endif ?>
            </div>
        </div>
    </div>
 
    <hr>
 
    <h2>Edit tasks</h2>
 
    <?php echo Form::model($tasks, array('method' => 'put', 'route' => array('admin.taskss.update', $tasks->id))) ?>
 
        <div class="control-group">
            <?php echo Form::label('title', 'Title') ?>
            <div class="controls">
                <?php echo Form::text('title') ?>
            </div>
        </div>
 
        <div class="control-group">
            <?php echo Form::label('body', 'Content') ?>
            <div class="controls">
                <?php echo Form::textarea('body') ?>
            </div>
        </div>
 
        <div class="form-actions">
            <?php echo Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) ?>
            <a href="<?php echo URL::route('admin.taskss.index') ?>" class="btn btn-large">Cancel</a>
        </div>
 
    <?php echo Form::close() ?>
 
</div>
</body>
</html>