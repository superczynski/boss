<?php require base_path().'/app/views/header.php';?>
<?php require base_path().'/app/views/navbar.php';?>
<div class="container">
 <br><br><br>
    <hr>
    <h2>Create new page</h2>
 
    <?php echo Form::open(array('route' => 'tasks.store')) ?>
 
        <div class="control-group">
            <?php echo Form::label('title', 'Title') ?>
            <div class="controls">
                <?php echo Form::text('title') ?>
            </div>
        </div>
 
        <div class="control-group">
            <?php echo Form::label('body', 'Content') ?>
            <div class="controls">
                <?php echo Form::textarea('body') ?>
            </div>
        </div>
 
        <div class="form-actions">
            <?php echo Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) ?>
            <a href="<?php echo URL::route('tasks.index') ?>" class="btn btn-large">Cancel</a>
        </div>
 
    <?php echo Form::close() ?>
 
</div>
</body>
</html>