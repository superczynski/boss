<?php

use Illuminate\Database\Migrations\Migration;

class TasksUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_tasks', function($table)
        {
        	$table->increments('id');
        	$table->string('nr_umowy');
            $table->string('pakiet');
            $table->string('opcje');
            $table->string('uwagi');
            $table->integer('status');
            $table->string('login_konsultanta');
            $table->timestamps();
            $table->string('login_koordynatora');
            $table->boolean('czy_wprowadzone');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}