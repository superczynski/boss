<?php

use Illuminate\Database\Migrations\Migration;

class LogTasks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log', function($table)
        {
        	$table->increments('id');
        	$table->string('task_id')->nullable();
            $table->string('who');
            $table->string('whom')->nullable();
            $table->string('event');
            $table->timestamps();
            $table->dropColumn('updated_at');


        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}