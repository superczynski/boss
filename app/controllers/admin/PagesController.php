<?php namespace App\Controllers\Admin;

use App\Models\Page;
use Input, Redirect, Sentry, Str;
 
class PagesController extends \BaseController {
 
    public function index()
    {
        return \View::make('admin.pages.index')->with('pages', Page::all());
    }
 
    public function show($id)
    {
        return \View::make('admin.pages.show')->with('page', Page::find($id));
    }
 
    public function create()
    {
        return \View::make('admin.pages.create');
    }
 
 
    public function edit($id)
    {
        return \View::make('admin.pages.edit')->with('page', Page::find($id));
    }
 
    public function update($id)
    {
        $page = Page::find($id);
        $page->title   = Input::get('title');
        $page->slug    = Str::slug(Input::get('title'));
        $page->body    = Input::get('body');
        $page->save();
        return Redirect::route('admin.pages.index');
    }
 


    public function store()
    {
        $page = new Page;
        $page->title   = Input::get('title');
        $page->slug    = Str::slug(Input::get('title'));
        $page->body    = Input::get('body');
        $page->user_id = '1';
        $page->save();
     
        return Redirect::route('admin.pages.index');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
     
        return Redirect::route('admin.pages.index');
    }



}
