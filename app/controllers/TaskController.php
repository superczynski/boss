<?php

class TaskController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('task.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		date_default_timezone_set('Europe/Warsaw');
		$task 						= new Task;
		$task->nr_umowy 			= HTML::entities(Input::get('umowa'));
		$task->pakiet 				= HTML::entities(Input::get('pakiet'));
		$task->uwagi 				= HTML::entities(Input::get('uwagi'));
		$task->status 				= '1';
		$task->login_konsultanta 	= 'dk100';
		$task->created_at 			= date("Y-m-d H:i:s");
		$task->updated_at 			= date("Y-m-d H:i:s");
		$task->login_koordynatora 	= 'ss0';
		$task->czy_wprowadzone 		= 'FALSE';
		$task->opcje 				= implode(',',Input::get('dodatkowe'));
		$task->save();
		//App::make('LogController')->log('task create', 'ss0');
		$log = new LogController;
		$log->log('task created', 'ss0', $task->id);

		return Redirect::route('task.index')->with('flash_notice', $task->nr_umowy);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}