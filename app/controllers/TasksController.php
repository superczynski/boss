<?php 

class TasksController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('tasks.index')->with('tasks', Task::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('tasks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return \View::make('tasks.edit')->with('tasks', Task::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{

	}
	public function updateTask($id)
	{
		 try
		 {
			date_default_timezone_set('Europe/Warsaw');
			$task 			= Task::find($id);
			$task->nr_umowy = strip_tags(Input::get('umowa'));
			$task->pakiet 	= strip_tags(Input::get('pakiet'));
			$task->uwagi 	= strip_tags(Input::get('uwagi'));
			if(Input::get('checkbox') == 1)
			{
				$task->status 			= '3';
				$task->czy_wprowadzone 	= 'TRUE';
			}
			else
			{
				$task->status 			= '2';
				$task->czy_wprowadzone 	= 'FALSE';
			}
			$task->login_konsultanta 	= 'dk100';
			$task->updated_at 			= date("Y-m-d H:i:s");
			$task->login_koordynatora 	= 'ss0';
			$task->opcje 				= implode(',',Input::get('dodatkowe'));
			$task->save();
			$log = new LogController;
			$log->log('task edited', 'ss0', $id);
			return $id." - successfuly closed";
		}
		catch(Exception $e)
		{
			return $id." - there was a problem with saving record";
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$task = Task::find($id);
        $task->delete();
     
        return Redirect::route('tasks.index');
	}

	public function getTask($id)
	{
		$task = Task::find($id);
        $task->status = '2';
        $task->save();
     	$log = new LogController;
		$log->log('task took', 'ss0', $id);
        return Redirect::route('tasks.index');
	}

	public function getId($id)
	{
    	$user = DB::table('user_tasks')->where('id', $id)->get();
		return $user;
	}

}