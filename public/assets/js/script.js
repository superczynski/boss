window.onload=function(){
    $('.selectpicker').selectpicker();
};

$(document).ready(function() {
   $("#myTable").tablesorter();
   $('.getId').click(function() {
      var id = $(this).attr('data-id');
       $.ajax({
           type: "GET",
             url: '/tasks/getId/'+id,             
             success: function(data){
              var id      = (data[0].id);
              var umowa   = (data[0].nr_umowy);
              var opcje   = (data[0].opcje);
              var pakiet  = (data[0].pakiet);
              var uwagi   = (data[0].uwagi);
              var valArr  = opcje.split(",");
              $("#umowa", '#myForm').val(umowa);
              $("#pakiet", '#myForm').val(pakiet);
              $("#uwagi", '#myForm').val(uwagi);
              $("#tempId", '#myForm').removeClass().addClass(""+id);
              $('select[name=dodatkowe]').val(valArr).change();
              $('#check_id').attr("checked",false);
            },
            error: function() {
                alert( "Error in connection");
             }
         });
     });
   
 $('#update').click(function() {
       var id         = $('#tempId').attr('class')
       var umowa      = $("#umowa", '#myForm').val();
       var pakiet     = $("#pakiet", '#myForm').val();
       var dodatkowe  = $('select[name=dodatkowe]', '#myForm').val();
       var uwagi      = $("#uwagi", '#myForm').val();
       var checkbox   = $('#check_id').is(':checked') ? 1 : 0;
       var user = $('#tempUser').attr('class');
       if(umowa && pakiet && dodatkowe){
       $.ajax({
           type: "GET"
,            url: '/tasks/updateTask/'+id, 
            data: {"id":id, "umowa":umowa, "pakiet":pakiet, "dodatkowe":dodatkowe, "uwagi":uwagi, "checkbox":checkbox, "user":user},
             success: function(data){
              if(checkbox ==1){
              var data_ok = '<div class=\"alert alert-success\">' +
                            '<a href=\"#\" class=\"alert-success\">Zamknieto zgloszenie</a></div>';
                  }else{
                  var data_ok = '<div class=\"alert alert-success\">' +
                                '<a href=\"#\" class=\"alert-success\">Zapisano zgloszenie</a></div>';      
                  }
                  $(".result").html(data_ok);
                 setTimeout(function(){
                    $('#myModal').modal('hide');
                    window.location.href = window.location.href.toString();
                  }, 1500);
                  // $(document).ajaxStop(function(){
                  //     window.location.reload();
                  // });
             },
             error: function() {
                 alert( "Error in connection");
             }
         });
     }else{
        var data_error = '<div class=\"alert alert-danger\">' +
                         '<a href=\"#\" class=\"alert-link\">Uzupelnij wszystkie pola</a></div>';
       $(".result").html(data_error);
       }
    }); 
}); 

    

 // });
  // $('.getTask').click(function() {
  //     var id = $(this).parent().parent().find("td:first").text();
  //     var user = $('#tempUser').attr('class');
  //     $.ajax({
  //         type: "POST",
  //           url: '../controller.php',
  //           data: {"getTaskId":id,"user":user},
  //           success: function(data){
  //             $(document).ajaxStop(function(){
  //                 window.location.reload();
  //             });
  //           },
  //           error: function() {
  //               alert( "Error in connection");
  //           }
  //       });
  //   });
//});